# Futzin Brabo - EP3

Italo Vinícius - 18/0102056 - Turma A (prof. Renato Coral)
Guilherme Richter - 18/0101617 - Turma B (prof. Carla Rocha)

## Descrição do projeto

Este projeto é uma idealização de uma rede social que tem o objetivo de marcar peladas, os famosos jogos amadores de futebol.
Há dois tipos de usuário: Jogador e Gerenciador e os dois podem ter vantagens de ADMIN.

## Versões utilizadas

```ruby
ruby '2.6.5'
rails '6.0.1'
yarn '1.19.2'
node '13.2.0'
```

## Execute no terminal

```
bundle install --without production
yarn install --check-files
rails db:migrate
rails s
```

## No seu navegador, entre no link

```
http://localhost:3000
```

