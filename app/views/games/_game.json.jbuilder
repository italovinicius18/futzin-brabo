json.extract! game, :id, :place, :date, :price, :size, :created_at, :updated_at
json.url game_url(game, format: :json)
