json.extract! manager, :id, :admin, :name, :cpf, :age, :phone, :created_at, :updated_at
json.url manager_url(manager, format: :json)
