json.extract! play, :id, :game, :player, :created_at, :updated_at
json.url play_url(play, format: :json)
