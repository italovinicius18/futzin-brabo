json.extract! user, :id, :admin, :name, :cpf, :age, :phone, :created_at, :updated_at
json.url user_url(user, format: :json)
