Rails.application.routes.draw do
  resources :plays
  get '/play/game' => 'plays#create'
  root to: "home#index"
  authenticated :user do
    root to: "games#index", as: :authenticated_root
  end
  devise_for :managers
  resources :games
  devise_for :users do
    get "/users/sign_out" => "devise/sessions#destroy", :as => :destroy_user_session
  end
  get "/game/play" => "plays#new"
  resources :managers
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
