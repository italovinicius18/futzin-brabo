class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.boolean :admin
      t.string :name
      t.string :cpf
      t.integer :age
      t.string :phone

      t.timestamps
    end
  end
end
