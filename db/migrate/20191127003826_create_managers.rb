class CreateManagers < ActiveRecord::Migration[6.0]
  def change
    create_table :managers do |t|
      t.boolean :admin
      t.string :name
      t.string :cpf
      t.integer :age
      t.string :phone

      t.timestamps
    end
  end
end
