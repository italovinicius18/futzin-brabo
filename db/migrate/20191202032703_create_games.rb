class CreateGames < ActiveRecord::Migration[6.0]
  def change
    create_table :games do |t|
      t.string :place
      t.datetime :date
      t.float :price
      t.integer :size

      t.timestamps
    end
  end
end
