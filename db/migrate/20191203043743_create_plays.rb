class CreatePlays < ActiveRecord::Migration[6.0]
  def change
    create_table :plays do |t|
      t.integer :game
      t.integer :player

      t.timestamps
    end
  end
end
